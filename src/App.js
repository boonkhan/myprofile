import React from "react";
import "./App.css";
import Navbar from "./components/Navbar";
import Home from "./contents/Home";
import Test2 from "./contents/Test2";
import { BrowserRouter as Router, Route } from "react-router-dom";
function App() {
     return (
          <Router>
               <div className="App">
                    <Navbar />
                    <Route exact path="/">
                         <Home />
                    </Route>
                    <Route exact path="/test2">
                         <Test2 />
                    </Route>
               </div>
          </Router>
     );
}

export default App;
