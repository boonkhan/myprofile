import React, { Component } from "react";
import { CircularProgressbar } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";

class Home extends Component {
     constructor(props) {
          super(props);
          this.state = {
               initial: 0,
          };
     }

     increase = () => {
          let initial = this.state.initial;
          if (initial >= 0 && initial < 100) {
               this.setState({
                    initial: initial + 5,
               });
          }
     };

     decrease = () => {
          let initial = this.state.initial;
          if (initial <= 100 && initial > 0) {
               this.setState({
                    initial: initial - 5,
               });
          }
     };

     render() {
          return (
               <div className="condiv home">
                    <div className="row">
                         <CircularProgressbar value={this.state.initial} text={`${this.state.initial}%`} />;
                         <div className="col-3 my-3">
                              <div className="input-number">
                                   <div className="col-6 " style={{ width: "50%" }}>
                                        <button onClick={() => this.increase()}>+</button>
                                   </div>
                                   <div className="col-6 " style={{ width: "50%" }}>
                                        <button onClick={() => this.decrease()}>-</button>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          );
     }
}

export default Home;
