import React, { Component } from "react";
class Test2 extends Component {
     constructor(props) {
          super(props);
          this.state = {
               triangle: [],
          };
     }
     componentDidMount() {
          this.triangle(6);
     }

     triangle(n) {
          let triangle = this.state.triangle;
          for (let i = 1; i <= n; i++) {
               let str2 = "X".repeat(i * 2 - 1);
               let p = str2;
               triangle.push(p);
          }
          this.setState({ triangle });
     }

     render() {
          return (
               <div className="condiv home ">
                    {this.state.triangle.map((e) => (
                         <div className="col-3 my-3">
                              <h1>{e}</h1>
                         </div>
                    ))}
               </div>
          );
     }
}
export default Test2;
